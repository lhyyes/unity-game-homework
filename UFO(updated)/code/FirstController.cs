﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstController : MonoBehaviour,ISceneController,UserAction
{
    // Start is called before the first frame update
    public GameObject[] terrain;
    public UFOController uc;
    public int score;
    public int lost;
    public yesyes[] yes;

    public GUIStyle style;

    public int round;



    void Start()
    {
        Direct.getInstance().currentScene=this;
        loadResources();
        Random.InitState((int)System.DateTime.Now.Ticks);
        uc.StartRound();
        score=0; lost=0;
        round=1;


    }

    // Update is called once per frame
    
    void Update()
    {
        if (round>=4) return;
        uc.UFOUpdate();

        if (uc.isOver()==1){
            round++;
            if (round>=4) 
                return;
            uc.setDifficulty(round);
            uc.StartRound();
        }  

    }

    void FixedUpdate(){
        uc.UFOFixedUpdate();
      
    }

    void OnGUI(){
        if (round>=4){
            style.alignment=TextAnchor.MiddleCenter;
            style.normal.textColor=Color.black;
            style.fontSize=250;
            if (score>lost){
                GUI.Label(new Rect(0,0,Screen.width,Screen.height),"你赢了",style);
            }else{
                GUI.Label(new Rect(0,0,Screen.width,Screen.height),"你输了",style);
            }
        }

        style.alignment=TextAnchor.MiddleLeft;
        style.normal.textColor=Color.black;
        style.fontSize=40;
        GUI.Label(new Rect(0,Screen.height-100,Screen.width,100),"白 1 分      蓝 2 分      红 3 分"+"                                     Score : "+score.ToString()+"       Lost : "+lost.ToString()+"              Round "+round.ToString(),style);
    
        for(int i=1;i<=33;++i){
            if (yes[i].isOn==1){
                if (Time.time-yes[i].stime>=3){
                    yes[i].isOn=0;
                    continue;
                }
                else{
                    if (yes[i].color=="white") style.normal.textColor=Color.gray;
                    if (yes[i].color=="blue") style.normal.textColor=Color.blue;
                    if (yes[i].color=="red") style.normal.textColor=Color.red;

                    GUI.Label(new Rect(yes[i].x,yes[i].y,400,200),"YesYes",style);
                }
            }
        }

    }

    public void loadResources(){
        terrain=new GameObject[4];
        terrain[0]=Instantiate(Resources.Load<GameObject>("Terrain"),new Vector3(0,0,0),Quaternion.identity);
        terrain[1]=Instantiate(terrain[0],new Vector3(-1000,0,0),Quaternion.identity);
        terrain[2]=Instantiate(terrain[0],new Vector3(-1000,0,-1000),Quaternion.identity);
        terrain[3]=Instantiate(terrain[0],new Vector3(0,0,-1000),Quaternion.identity);

        uc=new UFOController();
        yes=new yesyes[34];
        for (int i=0;i<=33;++i){
            yes[i]=new yesyes();
        }
    }

    public void getScore(int n){
        score+=n;

        for (int i=1;i<=33;++i){
            if (yes[i].isOn==0){
                yes[i].isOn=1;
                yes[i].x=Input.mousePosition.x;
                yes[i].y=Screen.height-Input.mousePosition.y;
                yes[i].stime=Time.time;

                if (n==1) yes[i].color="white";
                if (n==2) yes[i].color="blue";
                if (n==3) yes[i].color="red";

                break;
            }
        }
    }

    public void getLost(int n){
        lost+=n;
    }


}


public class yesyes{
    public int isOn;
    public float x,y;
    public float stime;
    public string color;

    public yesyes(){
        isOn=0;
        x=-200;y=-200;
        stime=-100;
        color="white";
    }

    public void over(){
        isOn=0;
    }
}