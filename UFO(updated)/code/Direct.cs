﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Direct : System.Object
{
    private static Direct instance;
    public ISceneController currentScene{get;set;}
    public static Direct getInstance(){
        if (instance==null)
            instance=new Direct();
        return instance;
    }

}

public interface ISceneController{
    
}
public interface UserAction{
    void getScore(int n);
    void getLost(int n);

}

public interface PAction{
    void addGravity();
    
}