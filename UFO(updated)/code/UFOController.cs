﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UFOController :MonoBehaviour,PAction
{
    
    public GameObject white,blue,red;
    public UFO[] ufos;
   
    public int diff;

    public float stime;

    private float lastCreateTime;
    public void UFOUpdate(){
        int number=getNumber();
        
        if (Time.time-stime>=9.5f){
            
        }else{
            if (Time.time-lastCreateTime>=1){
                lastCreateTime=Time.time;

                for (int i=1;i<=diff;++i){
                    CreateUFO();
                }
            }
        }

        for(int i=1;i<=33;++i){
            if (ufos[i].obj!=null){
                //ufos[i].obj.transform.position=Vector3.MoveTowards(ufos[i].obj.transform.position,ufos[i].target,ufos[i].speed);
                if (ufos[i].obj.transform.position==ufos[i].target || ufos[i].obj.transform.position.y<=4.5){
                    ufos[i].obj.GetComponent<UFOClass>().getOut();
                    number--;
                }
            }
        }
        
    }
    public void UFOFixedUpdate(){
        addGravity();
    }

    public void addGravity(){
        for(int i=1;i<=33;++i){
            if (ufos[i].obj!=null){
                ufos[i].obj.GetComponent<Rigidbody>().AddForce(new Vector3(0,-10,0));
            }
        }
    }

    public void StartRound(){
        stime=Time.time;
    }

    public int isOver(){
        if (getNumber()==0 && Time.time-stime>=10){
            return 1;
        }
        return 0;
    }

    public UFOController(){
        white=Resources.Load<GameObject>("whiteUFO");
        blue=Resources.Load<GameObject>("blueUFO");
        red=Resources.Load<GameObject>("redUFO");

        Rigidbody rb=new Rigidbody();

        white.AddComponent<Rigidbody>();
        white.GetComponent<Rigidbody>().detectCollisions=false;
        white.GetComponent<Rigidbody>().useGravity=false;
        white.GetComponent<Rigidbody>().mass=1;
        white.GetComponent<Rigidbody>().drag=0;
        white.GetComponent<Rigidbody>().angularDrag=10;

        blue.AddComponent<Rigidbody>();
        blue.GetComponent<Rigidbody>().detectCollisions=false;
        blue.GetComponent<Rigidbody>().useGravity=false;
        blue.GetComponent<Rigidbody>().mass=1;
        blue.GetComponent<Rigidbody>().drag=0;
        blue.GetComponent<Rigidbody>().angularDrag=10;

        red.AddComponent<Rigidbody>();
        red.GetComponent<Rigidbody>().detectCollisions=false;
        red.GetComponent<Rigidbody>().useGravity=false;
        red.GetComponent<Rigidbody>().mass=1;
        red.GetComponent<Rigidbody>().drag=0;
        red.GetComponent<Rigidbody>().angularDrag=10;

        ufos=new UFO[34];
        for (int i=0;i<34;++i){
            ufos[i]=new UFO();
        }
        diff=1;
        stime=-100;
        lastCreateTime=-100;
    }
    public void setDifficulty(int d){
        diff=d;
        
    }


    public int getNumber(){
        int ans=0;
        for (int i=1;i<=33;++i){
            if (ufos[i].obj!=null)
                ans++;
        }
        return ans;
    }

    public void CreateUFO(){
        UFO u=new UFO();
        for (int i=1;i<=33;++i){
            if (ufos[i].obj==null){
                u=ufos[i];
                break;
            }
        }


        u.init(diff);

        int kind;
        kind=Random.Range(0,100);
        if (kind<=33){
            u.obj=Instantiate(white,u.pos,Quaternion.identity);
            u.obj.name="white";
        }
        else if (kind<=66){
            u.obj=Instantiate(blue,u.pos,Quaternion.identity);
            u.obj.name="blue";
        }
        else{
            u.obj=Instantiate(red,u.pos,Quaternion.identity);
            u.obj.name="red";
        }

        u.initVelocity(diff);
        u.obj.AddComponent(typeof(UFOClass));
    }
}

public class UFO {
    public float speed;
    public Vector3 target;
    public GameObject obj;
    public Vector3 pos;
   
    public UFO(){

    }

    public void init(int diff){
        Object.Destroy(obj);
        float y=Random.Range(60,120f);
        float x=150;
        if (Random.Range(0,2)==0){
            x=-x;
        }
        pos=new Vector3(x,y,80);

        x=-x;
        y=Random.Range(60f,120f);
        target=new Vector3(x,y,80);
        

        speed=Random.Range(0.04f,0.06f);
        speed=speed*diff;


    }

    public void initVelocity(int diff){
        Vector3 dir=target-pos;
        dir=dir/dir.magnitude;
        obj.GetComponent<Rigidbody>().velocity=dir*diff*50;
    }
}


public class UFOClass:MonoBehaviour{
    private UserAction action;

    public UFOClass(){
        action=Direct.getInstance().currentScene as UserAction;
    }

    void OnMouseDown(){
        string s=gameObject.name;
        if (s=="white"){
            action.getScore(1);
        }
        if (s=="blue"){
            action.getScore(2);
        }
        if (s=="red"){
            action.getScore(3);
        }
        Destroy(gameObject);
        
    }

    public void getOut(){
        string s=gameObject.name;
        if (s=="white"){
            action.getLost(1);
        }
        if (s=="blue"){
            action.getLost(2);
        }
        if (s=="red"){
            action.getLost(3);
        }
        Destroy(gameObject);        
    }
}