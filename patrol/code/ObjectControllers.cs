﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeController:MonoBehaviour{
    public GameObject me;
    private float speed=10f;
    private float ac=7;
    private DataSource ds;
    private Animator ani;
    public Vector3 dir;
    public Vector3 v;

    void Start(){
        me=this.gameObject;
        ds=Director.GetCurrentScene() as DataSource;
        ani=me.GetComponent<Animator>();
        dir=new Vector3(0,0,0);
    }

    void FixedUpdate(){
        if (ds.GetStatus()==0) {
            me.GetComponent<Rigidbody>().velocity=new Vector3(0,0,0);
            return;
        }

        if (Input.GetKey(KeyCode.W)){
            if (me.GetComponent<Rigidbody>().velocity.z<=speed)
                me.GetComponent<Rigidbody>().AddForce(new Vector3(0,0,1)*ac);
            ani.SetBool("moving",true);
        }
        else{
           // if (me.GetComponent<Rigidbody>().velocity.z>0)
               // me.GetComponent<Rigidbody>().velocity-=new Vector3(0,0,me.GetComponent<Rigidbody>().velocity.z);
        }

//--------------------------------------------------------------------------------------------------
        if (Input.GetKey(KeyCode.S)){
            if (me.GetComponent<Rigidbody>().velocity.z>=0-speed)
                me.GetComponent<Rigidbody>().AddForce(new Vector3(0,0,-1)*ac);
            ani.SetBool("moving",true);
        }
        else{
           // if (me.GetComponent<Rigidbody>().velocity.z<0)
               // me.GetComponent<Rigidbody>().velocity-=new Vector3(0,0,me.GetComponent<Rigidbody>().velocity.z);
            
        }
//--------------------------------------------------------------------------------------------------------
        if (Input.GetKey(KeyCode.A)){
            if (me.GetComponent<Rigidbody>().velocity.x>=0-speed)
                me.GetComponent<Rigidbody>().AddForce(new Vector3(-1,0,0)*ac);
            ani.SetBool("moving",true);
        }
        else{
          //  if (me.GetComponent<Rigidbody>().velocity.x<0)
              //  me.GetComponent<Rigidbody>().velocity-=new Vector3(me.GetComponent<Rigidbody>().velocity.x,0,0);
        }
//-------------------------------------------------------------------------------------------------------
        if (Input.GetKey(KeyCode.D)){
            if (me.GetComponent<Rigidbody>().velocity.x<=speed)
                me.GetComponent<Rigidbody>().AddForce(new Vector3(1,0,0)*ac);
            ani.SetBool("moving",true);
        }
        else{
          //  if (me.GetComponent<Rigidbody>().velocity.x>0)
               // me.GetComponent<Rigidbody>().velocity-=new Vector3(me.GetComponent<Rigidbody>().velocity.x,0,0);
        }

//----------------------------------------------------------------------------------------------




        
    }

    void LateUpdate(){
        Vector3 s=me.GetComponent<Rigidbody>().velocity;
        if (new Vector3(s.x,0,s.z).magnitude<=5f){
            if (!Input.GetKey(KeyCode.W) && !Input.GetKey(KeyCode.S) &&!Input.GetKey(KeyCode.A) &&!Input.GetKey(KeyCode.D) )
                ani.SetBool("moving",false);
        }

        if ((s.x!=0 || s.z!=0) && s.magnitude>=0.1f){
            dir=new Vector3(s.x,0,s.z);
            dir=dir/dir.magnitude;
        }
        
        
        me.transform.LookAt(me.transform.position+dir-new Vector3(0,dir.y,0));

        v=me.GetComponent<Rigidbody>().velocity;


    }
}

public class PatrolController:MonoBehaviour{
    private GameObject patrol;
    private float scale=5;
    private float speed=5;
    private float ac=6;
    private DataSource ds;
    private ISceneController isc;

    public float[][] route;
    public int next;
    public int follow;

    void Start(){
        patrol=this.gameObject;
        route=CreateRoute();
        next=0;
        follow=0;

        ds=Director.GetCurrentScene() as DataSource;
        isc=Director.GetCurrentScene() as ISceneController;
    }

    void FixedUpdate(){     
        if (ds.GetStatus()==0){
            patrol.GetComponent<Rigidbody>().velocity=new Vector3(0,0,0);
            return;
        }

        Vector3 mp=ds.MePosition();   
        if ((mp-patrol.transform.position).magnitude<=10){
            Move(mp);
            follow=1;
        }
        else{
            if (follow==1){
                route=CreateRoute();
                next=0;
                follow=0;
                isc.GetScore();
            }
            Vector3 tar=new Vector3(route[next][0],patrol.transform.position.y,route[next][1]);
            Move(tar);        

            if ( (tar-patrol.transform.position).magnitude <=0.1f){
                next++;       
                if (next>3) next=0;
                
               
            }
        }



    }

    private float lc=0;
    void OnCollisionStay(Collision c){
        if (c.collider.name=="terrain" || ds.GetStatus()==0) return;

        if (c.collider.name=="wall"){
            if (Time.time-lc>=1f){
                if (follow==0) next++;
                if (next>3) next=0;
                lc=Time.time;
            }
        }

        if (c.collider.name=="me"){
            isc.GameOver();
        }
    }

    public float[][] CreateRoute(){

        float[][] ans;
        ans=new float[4][];
        for (int i=0;i<4;++i)   ans[i]=new float[2];

        float[] x=new float[4];
        float[] z=new float[4];

        x[0]=patrol.transform.position.x-scale; z[0]=patrol.transform.position.z-scale;
        x[1]=patrol.transform.position.x-scale; z[1]=patrol.transform.position.z+scale;
        x[2]=patrol.transform.position.x+scale; z[2]=patrol.transform.position.z-scale;
        x[3]=patrol.transform.position.x+scale; z[3]=patrol.transform.position.z+scale;

        for (int i=0;i<4;++i){
            if (x[i]<100) x[i]=100;
            if (x[i]>199) x[i]=199;

            if (z[i]<100) z[i]=100;
            if (z[i]>199) z[i]=199;
        }

        ans[0][0]=Random.Range(x[0],x[1]); ans[0][1]=Random.Range(z[0],z[1]);
        ans[1][0]=Random.Range(x[1],x[3]); ans[1][1]=Random.Range(z[1],z[3]);
        ans[2][0]=Random.Range(x[2],x[3]); ans[2][1]=Random.Range(z[2],z[3]);
        ans[3][0]=Random.Range(x[0],x[2]); ans[3][1]=Random.Range(z[0],z[2]);
        
        


        return ans;
    }


    public void Move(Vector3 target){

        target=new Vector3(target.x,patrol.transform.position.y,target.z);
        Vector3 dir=target-patrol.transform.position;
        float s=patrol.GetComponent<Rigidbody>().velocity.magnitude;
        if (s<speed)
            patrol.GetComponent<Rigidbody>().AddForce( dir/dir.magnitude *ac  );
        
        patrol.transform.LookAt(new Vector3(target.x,patrol.transform.position.y,target.z));
    }
}