﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Director : System.Object
{
    public static ISceneController current;
    public static ISceneController GetCurrentScene(){
        return current;
    }

}

public interface ISceneController{
    void LoadResources();
    void GameOver();
    void GetScore();
}

public interface DataSource{
    int GetStatus();
    Vector3 MePosition();
}