﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstController : MonoBehaviour,ISceneController,DataSource
{
    public GameObject me;
    public GameObject camera;
    public GameObject wall;
    public GameObject patrol;
    public GameObject hwall;
    public GameObject light;
    public Texture2D clo;
    public int patrolNum=17;
    public int wallNum=34;
    public int score=0;
    public int status=1;
    

    public GUIStyle f;

    // Start is called before the first frame update
    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);
        Director.current=this;
        LoadResources();

        f.fontSize=50;
        f.normal.textColor=new Color(255,255,255);
        f.alignment=TextAnchor.MiddleCenter;
    }

    // Update is called once per frame
    void Update()
    {
        camera.transform.position=me.transform.position+ new Vector3(0,12,-12);
        light.transform.position=me.transform.position+new Vector3(0,5,-5);
    }

    void OnGUI(){
        if (GUI.Button(new Rect(Screen.width-100,0,100,100),clo)){
            Application.Quit();
        }
        GUI.Label(new Rect(0,Screen.height-100,Screen.width,100),"Score : "+score.ToString(),f);
        
        if (status==0){
            GUI.Label(new Rect(Screen.width/2,Screen.height/2,200,100),"Game Over !",f);
        }
    }

    public void LoadResources(){
        Instantiate(Resources.Load<GameObject>("Terrain"),new Vector3(150,-0.5f,150),Quaternion.identity).name="terrain";
        me=Instantiate(Resources.Load<GameObject>("me"),new Vector3(150,1f,150),Quaternion.identity);
        me.name="me";
        me.AddComponent(typeof(MeController));
        camera=this.gameObject;
        wall=Resources.Load<GameObject>("wall");
        light=Instantiate(Resources.Load<GameObject>("light"),me.transform.position,Quaternion.identity);
        clo=Resources.Load<Texture2D>("15");

        for(int i=1;i<=101;++i){
            Instantiate(wall,new Vector3(99,1,99+i-1),Quaternion.identity).name="bianyuan";
            Instantiate(wall,new Vector3(99+i-1,1,200),Quaternion.identity).name="bianyuan";
            Instantiate(wall,new Vector3(200,1,200-i+1),Quaternion.identity).name="bianyuan";
            Instantiate(wall,new Vector3(200-i+1,1,99),Quaternion.identity).name="bianyuan";
        }

        patrol=Resources.Load<GameObject>("patrol");
        


        float[] x=new float[patrolNum+1];
        float[] z=new float[patrolNum+1];
        x[0]=150;z[0]=150;
        for (int i=1;i<=patrolNum;++i){
            while(true){
                x[i]=Random.Range(105f,194f);
                z[i]=Random.Range(105f,194f);

                bool can=true;
                for (int j=0;j<i;++j){
                    if ( (x[i]-x[j])*(x[i]-x[j])  +  (z[i]-z[j])*(z[i]-z[j]) <=3 ){
                        can=false;
                        break;
                    }
                }

                if (can==true){
                    GameObject t=Instantiate(patrol,new Vector3(x[i],1f,z[i]),Quaternion.identity);
                    t.name="patrol";
                    t.AddComponent(typeof(PatrolController));
                    break;
                }
            }
           
        }


        hwall=Resources.Load<GameObject>("h");
        for (int i=1;i<=wallNum;++i){
            float xx=Random.Range(101f,198f);
            float zz=Random.Range(101f,198f);

            GameObject t=Instantiate(hwall,new Vector3(xx,1,zz),Quaternion.identity);
            t.name="wall";
            if (Random.Range(0f,100f)<=50){
                t.transform.LookAt(t.transform.position+new Vector3(-1,0,0));
                
            }
        }

        
    }

    public int GetStatus(){
        return status;
    }
    public Vector3 MePosition(){
        return me.transform.position;
    }

    public void GameOver(){
        status=0;
    }
    public void GetScore(){
        score++;
    }
}
