﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyGUI : MonoBehaviour
{
    UserAction action;
    public int w,h;
    public Texture2D title,restart,over,win,esc;
    // Start is called before the first frame update
    void Start()
    {
        action=Direct.getInstance().currentScene as UserAction;
        w=Screen.width;
        h=Screen.height;

        title=Resources.Load<Texture2D>("img/title");
        restart=Resources.Load<Texture2D>("img/restart");
        over=Resources.Load<Texture2D>("img/over");
        win=Resources.Load<Texture2D>("img/win");
        esc=Resources.Load<Texture2D>("img/esc");
    }

    // Update is called once per frame
    void Update()
    {
        w=Screen.width;
        h=Screen.height;

        
    }

    void OnGUI(){
        

        GUI.Label(new Rect(w*0.26f,0,w*0.48f,w*0.12f),title);
        if ( GUI.Button(new Rect(w-100,0,100,100),esc)){
            Application.Quit();
        }
        if ( GUI.Button(new Rect(w*0.45f,h-w*0.1f,w*0.1f,w*0.05f),restart) ){
            action.restart();
        }
        
        if (Direct.gameStatus==-1){
            GUI.Label(new Rect(0.5f*w-100,0.5f*h-100,200,200),over);
        }
        if (Direct.gameStatus==1){
            GUI.Label(new Rect(0.5f*w-100,0.5f*h-100,200,200),win);
        }
    }
}
