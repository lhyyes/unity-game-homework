﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SController : MonoBehaviour
{
    public ParticleSystem ps;
    public ParticleSystem.Particle[] particles;

    public int speed=5;
    public float k=0.05f;
    public float b=1;
    public Transform tr;
    void Start()
    {
        ps=gameObject.GetComponent<ParticleSystem>();
        particles=new ParticleSystem.Particle[ps.maxParticles];
        tr.position=new Vector3(0,0,0);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        int num=ps.GetParticles(particles);
        float x,y,z;
        float r,nowr,rate;
        for (int i=0;i<num;++i){
            Vector3 v=particles[i].position;
            
            x=v.x;
            y=v.y;
            z=v.z;

            nowr=(new Vector3(0,y,0)-particles[i].position).magnitude;
            r=y*y*k+b;
            
            if (nowr<=0.5){
                x=1;
                z=0;
            }else{
                rate=r/nowr;
                x=x*rate;
                z=z*rate;
            }

            tr.position=new Vector3(x,y,z);
            
            tr.RotateAround(new Vector3(0,y,0),Vector3.up,speed);

            particles[i].position=tr.position;
        }
        ps.SetParticles(particles,num);
    }
}
